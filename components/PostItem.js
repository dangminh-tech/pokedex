import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

export default function PostItem({ item, navigation }) {
    return (
        <View style={styles.postHeader}>
            <TouchableOpacity
                style={{ padding: 15 }}
                onPress={() => navigation.navigate('PostDetail', {
                    id: item.id
                })}>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                    <Image source={require('../assets/img/elon-musk.png')} style={{ width: 50, height: 50, borderRadius: 25, marginRight: 15 }} />
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontWeight: '700', fontSize: 18, color: '#000' }}>{item?.username ? item.username : 'Bret'}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ marginRight: 10 }}>18 hours </Text>
                            <TouchableOpacity>
                                <Icon name={'earth'} size={20} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <Entypo name={'dots-three-horizontal'} size={20} />
                </View>

            </TouchableOpacity>
            <View style={{ width: '100%' }}>
                <TouchableOpacity onPress={() => navigation.navigate('PostDetail', {
                    id: item.id
                })}>
                    <Text style={{ fontSize: 15, marginHorizontal: 15, marginBottom: 10, textAlign: 'justify' }}>{item?.body}</Text>

                </TouchableOpacity>
                <Image source={require('../assets/img/elon-musk.png')} style={{ width: '100%' }} />
            </View>
            <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={require('../assets/img/like.png')} style={{ width: 40, height: 40, zIndex: 2 }} />
                    <Image source={require('../assets/img/haha.png')} style={{ width: 30, height: 30, position: 'relative', left: -10, zIndex: 1 }} />
                    <Text>6.517</Text>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity>
                        <Text>109 comments</Text>
                    </TouchableOpacity>
                    <Entypo name={'dot-single'} size={20} />
                    <TouchableOpacity>
                        <Text>6 shares</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View
                style={
                    {
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        padding: 10,
                        borderTopWidth: 1,
                        borderColor: '#ccc',
                        marginHorizontal: 15
                    }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15 }}>
                    <AntDesign name={'like2'} size={30} />
                    <Text style={{ paddingLeft: 5 }}>Like</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15 }}>
                    <EvilIcons name={'comment'} size={30} />
                    <Text style={{ paddingLeft: 5 }}>Comment</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15 }}>
                    <FontAwesome5 name={'share'} size={30} />
                    <Text style={{ paddingLeft: 5 }}>Share</Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        maxWidth: 350
    },
    postHeader: {
        backgroundColor: '#fff',
        // elevation: 5,
        marginBottom: 20
    }
})