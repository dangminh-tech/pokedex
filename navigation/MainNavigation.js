import { View, Text } from 'react-native'
import React from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Tabbar from './Tabbar';
import PostDetail from '../screens/btAxios/PostDetail';
import UserDetail from '../screens/btAxios/UserDetail';
import AlbumDetail from '../screens/btAxios/AlbumDetail';
import Home from '../screens/pokemon/Home.js';


const Stack = createNativeStackNavigator();


export default function MainNavigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator >
                <Stack.Screen options={{ headerShown: false }} name='Main' component={Tabbar} />
                <Stack.Screen
                    name='PostDetail'
                    component={PostDetail}
                />
                <Stack.Screen
                    name='UserDetail'
                    component={UserDetail}
                    options={({ route }) => ({ title: `${route.params.username}` })}
                />
                <Stack.Screen
                    name='AlbumDetail'
                    component={AlbumDetail}
                />
                {/* <Stack.Screen name='Home' component={Home} /> */}
            </Stack.Navigator>
        </NavigationContainer>
    )
}