import { View, Text } from 'react-native'
import React from 'react'
import PostsScreen from '../screens/btAxios/PostsScreen'
import UsersScreen from '../screens/btAxios/UsersScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5'

const Tab = createBottomTabNavigator();

export default function Tabbar() {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => { //NOTES: HIển thị icon ở tabbar
                    let iconName;

                    if (route.name === 'Posts') {
                        iconName = 'home'
                    } else if (route.name === 'Users') {
                        iconName = 'bars'
                    }

                    return <Icon name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: '#00B2FF',
                tabBarInactiveTintColor: 'gray',
                headerShown: false
            })}>
            <Tab.Screen name="Posts" component={PostsScreen} />
            <Tab.Screen name="Users" component={UsersScreen} />
        </Tab.Navigator>
    )
}