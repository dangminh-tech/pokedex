import { View, Text, SafeAreaView } from 'react-native'
import React, { useEffect } from 'react'
import MainNavigation from './navigation/MainNavigation';



export default function App() {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <MainNavigation />
    </SafeAreaView>
  );
}