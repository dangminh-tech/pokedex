import api from './AxiosPokemonConfig'

class APIPokemon {
    getPokemon() {
        api.get('/pokemon/')
    }
}

export default new APIPokemon();