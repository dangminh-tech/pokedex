import api from './AxiosConfig'

class APIService {
    getPost() {
        return api.get('./posts')
    }

    getPostDetail(id) {
        return api.get('/posts/' + id)
    }

    getPostComments(id) {
        return api.get('/posts/' + id + '/comments')
    }

    getUsers() {
        return api.get('./users')
    }

    getUser(id) {
        return api.get('./users/' + id)
    }

    getAlbums() {
        return api.get('./albums')
    }

    getPhotos(id) {
        return api.get('./photos?albumId=' + id)
    }

    getTodos() {
        return api.get('./todos')
    }
}

export default new APIService();