import { View, Text, FlatList, ScrollView, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import Api from '../../Networking/Api'
import PostItem from '../../components/PostItem';
import AntDesign from 'react-native-vector-icons/AntDesign'


export default function PostDetail({ route }) {
    const { id } = route.params
    const [post, setPost] = useState();
    const [comments, setComments] = useState();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetch()
    }, [])
    const fetch = () => {
        setLoading(true)
        Api.getPostDetail(id)
            .then(res => setPost(res.data))
            .finally(() => setLoading(false))
        Api.getPostComments(id)
            .then(res => setComments(res.data))
            .finally(() => setLoading(false))
    }
    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
            <PostItem item={post} />
            <Text style={{ fontSize: 24, fontWeight: '700', marginVertical: 10, marginLeft: 15 }}>Comments</Text>
            <FlatList
                data={comments}
                refreshing={loading}
                onRefresh={fetch}
                renderItem={({ item }) => {
                    return (
                        <View style={{
                            paddingTop: 15,
                            borderColor: '#ccc',
                            marginHorizontal: 15,
                            backgroundColor: '#fff',
                            flexDirection: 'row'
                        }}>
                            <Image source={require('../../assets/img/avatar.webp')} style={{ width: 70, height: 70, borderRadius: 50 }} />
                            <View style={{ marginLeft: 10 }}>
                                <View style={{ backgroundColor: '#f5f5f5', padding: 10, borderRadius: 15 }}>
                                    <View style={{ width: 340 }}>
                                        <Text style={{ fontSize: 16, fontWeight: '800' }}>{item.name}</Text>
                                        <Text style={{ fontSize: 16, textAlign: 'justify' }}>{item.body}</Text>
                                    </View>
                                </View>

                                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}> */}
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 180, marginTop: 10 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 15, fontWeight: '700', marginRight: 15 }}>1 week</Text>
                                        <Text style={{ fontSize: 15, fontWeight: '700', marginRight: 15 }}>Like</Text>
                                        <Text style={{ fontSize: 15, fontWeight: '700' }}>Response</Text>
                                    </View>
                                    <View style={{ position: 'relative', left: 150, top: -10 }}>
                                        <Image source={require('../../assets/img/like.png')} style={{ width: 40, height: 40, zIndex: 2 }} />
                                    </View>


                                </View>
                                <View>

                                </View>
                                {/* </View> */}

                            </View>


                        </View>
                    )
                }}
            />
        </ScrollView>
    )
}