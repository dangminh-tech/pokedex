import { View, Text, FlatList, TouchableOpacity, useWindowDimensions, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import Api from '../../Networking/Api';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo'



import { TabView, SceneMap } from 'react-native-tab-view';


const TodoItem = ({ item }) => {
    const [check, setCheck] = useState(item.completed);
    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 15,
                borderWidth: 1,
                borderColor: '#ccc',
                borderRadius: 15,
                backgroundColor: '#fff',
                elevation: 5,
                marginBottom: 15
            }}
        >
            <Text style={{ fontSize: 18, width: 350 }} numberOfLines={1}>{item.title}</Text>
            {
                check ? (
                    <TouchableOpacity onPress={() => setCheck(!check)} style={{ backgroundColor: 'green', borderRadius: 20, height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name={'check'} size={30} color={'#fff'} />
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity onPress={() => setCheck(!check)} style={{ borderRadius: 20, height: 40, width: 40, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: '#ccc' }}>
                        {/* <Icon name={'check'} size={30} color={'#fff'} /> */}
                    </TouchableOpacity>
                )
            }

        </View>
    )
}

const Todos = ({ data, navigation }) => {
    return (
        <FlatList
            data={data}
            style={{ padding: 15 }}
            renderItem={({ item }) => {
                return <TodoItem item={item} />
            }}
        />
    )
};

const Albums = ({ data, navigation }) => {
    return (
        <FlatList
            data={data}
            numColumns={2}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => {
                return (
                    <TouchableOpacity
                        style={{
                            borderColor: '#ccc',
                            backgroundColor: '#fff',
                            marginBottom: 15,
                            marginLeft: 28, marginTop: 15
                        }}
                        onPress={() => navigation.navigate('AlbumDetail', { id: item.id })}
                    >
                        <Image source={require('../../assets/img/avatar.webp')} style={{ width: 200, height: 200, borderRadius: 10 }} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ paddingLeft: 5, paddingTop: 5 }}>
                                <Text numberOfLines={1} style={{ width: 100, fontWeight: '700', fontSize: 15 }}>{item.title}</Text>
                                <Text>10 photos</Text>
                            </View>
                            <Entypo name='dots-three-vertical' size={20} />
                        </View>
                    </TouchableOpacity>
                )
            }}
        />
    )
}



export default function UserDetail({ route, navigation }) {
    const { id } = route.params
    const [user, setUser] = useState({});
    const [todos, setTodos] = useState([]);
    const [albums, setAlbums] = useState([]);

    useEffect(() => {
        Api.getUser(id)
            .then(res => setUser(res.data))
        Api.getAlbums()
            .then(res => setAlbums(res.data.filter(item => item.userId == id)))
        Api.getTodos()
            .then(res => setTodos(res.data.filter(item => item.userId == id)))
    }, [])

    const layout = useWindowDimensions();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Todos' },
        { key: 'second', title: 'Albums' },
    ]);

    const renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <Todos data={todos} navigation={navigation} />;
            case 'second':
                return <Albums data={albums} navigation={navigation} />;
            default:
                return null;
        }
    };

    return (
        <>
            <View style={{ alignItems: 'center', marginVertical: 15 }}>
                <Image source={require('../../assets/img/avatar.webp')} style={{ height: 200, width: 200, borderRadius: 100 }} />
                <Text style={{ fontSize: 20, fontWeight: '700', marginVertical: 5 }}>{user?.name}</Text>
                <Text style={{ fontSize: 18, fontWeight: '500', marginVertical: 5 }}>{user?.username}</Text>
                <Text style={{ fontSize: 18, fontWeight: '500', marginVertical: 5 }}>{user?.email}</Text>
            </View>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                style={{ backgroundColor: '#fff' }}
            />
        </>
    )
}