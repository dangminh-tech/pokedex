import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import Api from '../../Networking/Api'
import PostItem from '../../components/PostItem'





export default function PostsScreen({ navigation }) {
    const [posts, setPosts] = useState([])
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        fetch()
    }, [])
    const fetch = () => {
        setLoading(true)
        Api.getPost()
            .then(res => {
                setPosts(res.data)
            })
            .finally(() => setLoading(false))
        Api.getUsers()
            .then(res => {
                setUsers(res.data)
            })
            .finally(() => setLoading(false))
    }

    useEffect(() => {
        users.forEach(user => {
            posts.forEach(post => {
                if (user.id === post.userId) {
                    post.username = user.username
                }
            })
        })
    }, [posts])


    return (
        <View>
            <Text style={{ fontSize: 25, fontWeight: '700', backgroundColor: '#00B2FF', padding: 15, color: '#fff' }}>Newsfeed</Text>
            <FlatList
                data={posts}
                refreshing={loading}
                onRefresh={fetch}
                renderItem={({ item, index }) => {
                    return <PostItem item={item} navigation={navigation} />
                }}
            />
        </View>
    )
}

