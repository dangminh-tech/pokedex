import { View, Text, TextInput, FlatList, TouchableOpacity, StyleSheet, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import Api from '../../Networking/Api'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'


export default function UsersScreen({ navigation }) {
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(true)
    const fetch = () => {
        setLoading(true)
        Api.getUsers()
            .then(res => setUsers(res.data))
            .finally(() => setLoading(false))
    }

    useEffect(() => {
        fetch()
    }, [])
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Text style={{ fontSize: 25, fontWeight: '700', backgroundColor: '#00B2FF', padding: 15, color: '#fff' }}>Users</Text>
            <View style={{ position: 'relative', paddingHorizontal: 15, marginTop: 15 }}>
                <Ionicons name={'search'} size={30} style={{ position: 'absolute', zIndex: 999, left: 25, top: 10 }} />
                <TextInput style={{ backgroundColor: '#f5f5f5', borderRadius: 20, paddingLeft: 50 }} placeholder='Find friend' />
            </View>
            <Text style={{ fontSize: 23, fontWeight: '700', color: '#000', paddingVertical: 10, paddingLeft: 15 }}>{users?.length} users</Text>
            <FlatList
                data={users}
                style={{ padding: 15 }}
                refreshing={loading}
                onRefresh={fetch}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableOpacity
                            style={styles.itemUser}
                            onPress={() => navigation.navigate('UserDetail', {
                                id: item.id,
                                username: item.username
                            })}>
                            <Image source={require('../../assets/img/avatar.webp')} style={{ width: 100, height: 100, marginRight: 5, borderRadius: 50 }} />
                            <View style={{ paddingLeft: 15, flex: 1 }}>
                                <Text style={{ fontSize: 22, fontWeight: '700', color: '#000' }}>{item.username}</Text>
                                <Text style={[styles.text]} numberOfLines={1}>
                                    {`${item.website}`}
                                </Text>
                            </View>
                            <Entypo name={'dots-three-horizontal'} size={20} />
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        maxWidth: 320
    },

    itemUser: {
        marginBottom: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center'
    }
})