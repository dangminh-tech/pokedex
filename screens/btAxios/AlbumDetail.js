import { View, Text, FlatList, Image, useWindowDimensions } from 'react-native'
import React, { useState, useEffect } from 'react'
import Api from '../../Networking/Api'

export default function AlbumDetail({ route }) {
    const { id } = route.params
    const { width, height } = useWindowDimensions()
    const [photos, setPhotos] = useState()
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        fetch()
    }, [])

    const fetch = () => {
        setLoading(true)
        Api.getPhotos(id)
            .then(res => setPhotos(res.data))
            .finally(() => setLoading(false))
    }
    return (
        <View>
            <FlatList
                data={photos}
                refreshing={loading}
                onRefresh={fetch}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10, justifyContent: 'space-between' }}>

                            {
                                index % 2 == 0 ? (
                                    <>
                                        <Image source={{ uri: item.url }} style={{ width: (width - 30) * 6 / 10 - 5, height: (width - 30) * 7 / 10 }} />
                                        <Image source={{ uri: item.thumbnailUrl }} style={{ width: (width - 30) * 4 / 10 - 5, height: (width - 30) * 7 / 10 }} />
                                    </>

                                ) : (
                                    <>
                                        <Image source={{ uri: item.url }} style={{ width: (width - 30) * 4 / 10 - 5, height: (width - 30) * 7 / 10 }} />
                                        <Image source={{ uri: item.thumbnailUrl }} style={{ width: (width - 30) * 6 / 10 - 5, height: (width - 30) * 7 / 10 }} />
                                    </>
                                )
                            }
                        </View>
                    )
                }}
            />
        </View>
    )
}